//
//  ListaViewController.swift
//  MyApp
//
//  Created by Charles Franca on 15/07/19.
//  Copyright © 2019 Charles Franca. All rights reserved.
//

import UIKit

class ListaViewController: UIViewController {
    let lista = [
        "Ïtem 1", "Item 2", "Item 3", "Item 4"
    ]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}

extension ListaViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lista.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listaCell") as! ListaTableViewCell
        
        let item = lista[indexPath.row]
        cell.lblDesc.text = item
        
        return cell
    }
}
