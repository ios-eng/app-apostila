//
//  MapViewController.swift
//  MyApp
//
//  Created by Charles Franca on 15/07/19.
//  Copyright © 2019 Charles Franca. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
    var locationManager = CLLocationManager()
    
    @IBOutlet weak var map: MKMapView!
    @IBAction func UsersLocation(_ sender: Any) {
        GetUserLocation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
    }
    
    func GetUserLocation() {
        
        let location = locationManager.location
        print(location!)
        let userCenter = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)

        let region = MKCoordinateRegion(center: userCenter, span: MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005))
        
        map.setRegion(region, animated: true)
    }

}

extension MapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            print("Not determined")
        case .restricted:
            break
        case .denied:
            break
        case .authorizedAlways:
            break
        case .authorizedWhenInUse:
            break
        @unknown default:
            break
        }
    }
}
