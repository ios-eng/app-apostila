//
//  ViewController.swift
//  MyApp
//
//  Created by Charles Franca on 15/07/19.
//  Copyright © 2019 Charles Franca. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var txtName: UITextField!
    @IBAction func GetTypedName(_ sender: Any) {
        lblName.text = txtName.text ?? ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}

