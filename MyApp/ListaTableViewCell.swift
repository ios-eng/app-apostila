//
//  ListaTableViewCell.swift
//  MyApp
//
//  Created by Charles Franca on 15/07/19.
//  Copyright © 2019 Charles Franca. All rights reserved.
//

import UIKit

class ListaTableViewCell: UITableViewCell {
    @IBOutlet weak var lblDesc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
